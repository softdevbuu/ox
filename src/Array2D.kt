fun main() {
    val array2d1 = arrayOf(
        arrayOf( 1, 2, 3, 4, 5),
        arrayOf( 6, 7, 8, 9),
        arrayOf( 11, 12, 13),
        arrayOf( 16, 17),
        arrayOf( 21)
    )

    for( row in array2d1 ) {
        for( col in row ) {
            print("$col ")
        }
        println()
    }
    array2d1[1][1] = 0
    for( i in array2d1.indices ) {
        for( j in array2d1[i].indices ) {
            print("${array2d1[i][j]} ")
        }
        println()
    }

    val table = arrayOf(
        arrayOf(' ', '1', '2', '3'),
        arrayOf('1', '-', '-', '-'),
        arrayOf('2', '-', '-', '-'),
        arrayOf('3', '-', '-', '-')
    )
    println("Welcome to OX Game")
    for( row in table ) {
        for( col in row ) {
            print(col)
        }
        println()
    }
    // input X row = 2 col = 2
    val rowInput = 2
    val colInput = 2
    table[rowInput][colInput] = 'X'
    for( row in table ) {
        for( col in row ) {
            print(col)
        }
        println()
    }

    val array2d2 = Array(5, {Array(5, {0})})
    for( row in array2d2) {
        for(col in row) {
            print("$col ")
        }
        println()
    }
}