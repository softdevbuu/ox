import kotlin.system.exitProcess
var rowInd = 0
var colInd = 0
fun input() {
    while(true) {
        try {
            print("Please input R C: ")
            val input = readLine()
            println(input)
            val rcList = input?.split(" ")
            if( rcList?.size != 2 ) {
                println("Error: Must input 2 numbers R C (Ex: 1 2)")
                continue
            }
            println("Row ${rcList[0]} Col  ${rcList[1]}")
            rowInd = rcList[0].toInt()
            colInd = rcList[1].toInt()
            println("Row $rowInd Col  $colInd")
            break
        } catch (t: Throwable) {
            println("Error: ${t.message} ,Must be numbers R C (Ex: 1 2)")
        }

    }
}

fun main() {

    input()
}